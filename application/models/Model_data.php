<?php
class Model_data extends CI_Model {

    var $table = 'muser';
    var $column = array('username','kategori','kotaKab'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $column_search = array('username','kategori','kotaKab');
    var $order = array('id' => 'desc'); // default order

    function __construct(){
        parent::__construct();
    }

    public function getdata($param)
    {
        $query = $this->db->query("select * from $param order by id desc")->result();
        return $query;
    }

    public function getfile($id, $type)
    {
        $query = $this->db->query("select * from data_file where id_parent = '$id' and type = '$type' order by id asc")->result();
        return $query;
    }

    public function createdata($table, $params = NULL)
    {
        $this->db->insert($table, $params);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function getwhere($field, $from, $where)
    {
        $query = $this->db->query("select $field from $from where $where order by id asc")->result();
        return $query;
    }

    public function getwherefilter($field, $from, $where)
    {
        $query = $this->db->query("select $field from $from where $where")->result();
        return $query;
    }

    public function updateberita($params = NULL)
    {
        $valid = true;
       
        $this->db->set('stat', $params->stat);
        $this->db->set('update_by', $params->update_by);
        $this->db->set('update_date', $params->update_date);
        $this->db->where('id', $params->id);
        $this->db->update('data_berita');
        
        return $valid;

    }

    public function updatejadwalstat($params = NULL)
    {
        $valid = true;
       
        $this->db->set('status', $params->stat);
        $this->db->set('update_by', $params->update_by);
        $this->db->set('update_date', $params->update_date);
        $this->db->where('id', $params->id);
        $this->db->update('data_jadwal');
        
        return $valid;

    }

    public function updatedataberita($params = NULL)
    {
        $valid = true;
       
        $this->db->set('judul', $params->judul);
        $this->db->set('tag', $params->tag);
        $this->db->set('isi', $params->isi);
        $this->db->set('stat', $params->stat);
        $this->db->set('update_by', $params->update_by);
        $this->db->set('update_date', $params->update_date);
        $this->db->where('id', $params->id);
        $this->db->update('data_berita');
        
        return $valid;

    }

    public function updatedatalelang($params = NULL)
    {
        $valid = true;
        
        $this->db->set('judul_paket',  $params->judul_paket);
        $this->db->set('pelaksana',  $params->pelaksana);
        $this->db->set('target_lelang',  $params->target_lelang);
        $this->db->set('tanggal_mulai',  $params->tanggal_mulai);
        $this->db->set('tanggal_akhir',  $params->tanggal_akhir);
        // $this->db->set('tanggal_kontrak',  $params->tanggal_kontrak);
        $this->db->set('status',  $params->status);
        $this->db->set('link',  $params->link);
        // $this->db->set('deskripsi',  $params->deskripsi);
        // $this->db->set('rekomendasi',  $params->rekomendasi);
        $this->db->set('tahun',  $params->tahun);
        $this->db->set('pagu_hps',  $params->pagu_hps);
        $this->db->set('hps',  $params->hps);
        $this->db->set('metode_lelang',  $params->metode_lelang);
        $this->db->set('keterangan',  $params->keterangan);
        $this->db->set('nilai_tayang',  $params->nilai_tayang);
        $this->db->set('nama_pelaksana',  $params->nama_pelaksana);
        $this->db->set('jenis_lelang',  $params->jenis_lelang);
        $this->db->set('tanggal_akhir_pekerjaan',  $params->tanggal_akhir_pekerjaan);
        
        $this->db->set('update_by', $params->update_by);
        $this->db->set('update_date', $params->update_date);
        $this->db->where('id', $params->id);
        $this->db->update('data_lelang');

        // print_r($this->db->last_query());die;
        
        return $valid;

    }

    public function updatedatajadwal($params = NULL)
    {
        $valid = true;
       
        $this->db->set('event', $params->event);
        $this->db->set('tanggal', $params->tanggal);
        $this->db->set('status', $params->status);
        $this->db->set('update_by', $params->update_by);
        $this->db->set('update_date', $params->update_date);
        $this->db->where('id', $params->id);
        $this->db->update('data_jadwal');
        
        return $valid;

    }
    
    public function updatePeta($params = NULL)
    {
        $valid = true;
       
        $this->db->set('latitude', $params->latitude);
        $this->db->set('longitude', $params->longitude);
        $this->db->set('nama', $params->nama);
        $this->db->set('alamat', $params->alamat);
        $this->db->set('kabupaten', $params->kabupaten);
        $this->db->set('kecamatan', $params->kecamatan);
        $this->db->set('desa', $params->desa);
        $this->db->set('kapasitas', $params->kapasitas);
        $this->db->set('luas', $params->luas);
        $this->db->set('fisik', $params->fisik);
        $this->db->set('keuangan', $params->keuangan);

        $this->db->set('update_by', $params->update_by);
        $this->db->set('update_date', $params->update_date);
        $this->db->where('id', $params->id);
        $this->db->update('data_peta');
        
        return $valid;

    }

    public function updateCriteria($params = NULL)
    {
        $valid = true;
        $table = $params->table;
        unset($params->table);
        $this->db->where('id', $params->id); 
        $this->db->update($table, $params);
        // print_r($this->db->last_query());die;
        return $valid;

    }

    public function updateLaporan($params = NULL)
    {
        $valid = true;
        
        $this->db->where('id', $params->id); 
        $this->db->update('data_laporan', $params);
        // print_r($this->db->last_query());die;
        return $valid;

    }

    public function updatePengaduan($params = NULL)
    {
        $valid = true;
        
        $this->db->where('id', $params->id); 
        $this->db->update('data_pengaduan', $params);
        // print_r($this->db->last_query());die;
        return $valid;

    }

    public function updatefile($params = NULL)
    {
        $valid = true;
        
        $this->db->set('type', $params['type']);
        $this->db->set('path', $params['path']);
        $this->db->set('size', $params['size']);
        $this->db->set('extension', $params['extension']);
        $this->db->set('filename', $params['filename']);
        $this->db->set('update_date', $params['update_date']);
        $this->db->where('id', $params['id']);
        $this->db->update('data_file');
        
        
        return $valid;

    }

    public function deletejadwal($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_jadwal');
    }

    public function deletefoto($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_foto');
    }

    public function deletebanner($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_banner');
    }

    public function deletevideo($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_video');
    }

    public function deletelaporan($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_laporan');
    }

    public function deletepengaduan($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_pengaduan');
    }

    public function deletepeta($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_peta');
    }
    public function deletecriteria($param)
    {
        $this->db->where('id', $param->id);
        $this->db->delete($param->table);
    }

    public function deleteberita($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_berita');
    }

    public function deletelelang($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id);
        $this->db->delete('data_lelang');
    }

    public function deletefile($id)
    {
        // $idx = $this->db->escape_str($id);
        $this->db->where('id', $id->id_file);
        $this->db->delete('data_file');
    }

    public function gettpst($param = null)
    {   
        $query = $this->db->query("select pu.id, pu.tpst from data_profil_usulan pu
                                   where pu.id not in (select tpst from $param where tpst = pu.id) order by pu.id asc")->result();
        return $query;
    }

    public function gettpstbyid($param = null, $id = null)
    {   
        $query = $this->db->query("select pu.id, pu.tpst from data_profil_usulan pu
                                   where pu.id = $id")->row_array();
        return $query;
    }
    
    public function countpaket($tahun = null, $param = null, $status = null )
    {   
        if($status){
            $where = " and status = '$status'";
        }

        if($param == 'pagu'){
            $query = $this->db->query("SELECT sum(pagu_hps) as total from data_lelang where tahun = '$tahun' $where")->result();
        }else if($param == 'hps'){
            $query = $this->db->query("SELECT sum(hps) as total from data_lelang where tahun = '$tahun' $where")->result();
        }else{
            $query = $this->db->query("SELECT count(*) as total from data_lelang where tahun = '$tahun' $where")->result();
        }
        return $query;
    }

        
    public function getgrafik($tahun = null, $param = null, $status = null )
    {   

        $query = $this->db->query("SELECT count(*) as total from data_lelang where tahun = '$tahun' and pelaksana = '$param' and status = '$status'")->result();
        return $query;
    }

    public function getdatalelang($tahun = null, $param = null)
    {   

        $query = $this->db->query("SELECT * from data_lelang where tahun = '$tahun' and pelaksana = '$param' and status = '2'")->result();
        return $query;
    }

    public function updatestatuskeuangan($params = NULL)
    {
        $valid = true;
       
        $this->db->set('update_by', $params->update_by);
        $this->db->set('update_date', $params->update_date);
        $this->db->set($params->field, $params->value);
        $this->db->where('param', $params->param);
        $this->db->update('data_status_keuangan');
        
        return $valid;

    }

    public function updateFoto($params = NULL)
    {
        $valid = true;
        
        $this->db->where('id', $params->id); 
        $this->db->update('data_foto', $params);
        // print_r($this->db->last_query());die;
        return $valid;

    }

    public function updateBanner($params = NULL)
    {
        $valid = true;
        
        $this->db->where('id', $params->id); 
        $this->db->update('data_banner', $params);
        // print_r($this->db->last_query());die;
        return $valid;

    }

}
