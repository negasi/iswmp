$(function () {
    console.log('You are running jQuery version: ' + $.fn.jquery);
    $('#statuskeuangan > a').attr('class','nav-link active')
    loaddata()

    $('input.form-control').keypress(function(event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
})

function loaddata(){
      
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: 'getglobal',
        data : {
                param       : 'data_status_keuangan',
                type        : 'status_keuangan',
        },
        success: function(result){
            var data = result.data
            for (let i = 0; i < data.length; i++) {
                const element = data[i];
                const par = element.param;
                for (var e in element) {
                    if(
                        e != 'id' &&
                        e != 'param' &&
                        e != 'create_date' &&
                        e != 'update_date' &&
                        e != 'create_by' &&
                        e != 'update_by'
                    ){
                        $(`#${par+'-'+e.replace('n','')}`).val(element[e])
                    }
                }
                
            }
  
        }
    });
}

function updatestat(isthis, param, n) {
    var formData = new FormData();
    formData.append('param', param);
    formData.append('field', `n${n}`);
    formData.append('value', isthis.value);
    $.ajax({
        type: 'post',
        url: 'updatestatuskeuangan',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        async:false,
        success: function(result){
            loaddata()
          }
    });
}

