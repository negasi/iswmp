$(function () {

  console.log('You are running jQuery version: ' + $.fn.jquery);
  $('[name="summernote"]').summernote({
    height: 200,   //set editable area's height
    codemirror: { // codemirror options
      theme: 'monokai'
    }
  });
  $('.select2').select2();
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })

  //Date picker
  $('#reservationdate').datetimepicker({
    format: 'L'
  });

  var st = true;
  window.img = '';
  $("input[data-bootstrap-switch]").each(function(){
    // $(this).bootstrapSwitch('state', $(this).prop('checked'));
    $(this).bootstrapSwitch({
      onSwitchChange: function(e, state) {
        st = state;
      }
    });
  });

  $('#listvideo').DataTable();
  
  $('#modal-default').on('show.bs.modal', function(){
  })

  $('.bootstrap-switch-handle-on').html('Ya');
  $('.bootstrap-switch-handle-off').html('Tidak');

  $('#pengaduan > a').attr('class','nav-link active');


  $('#add-laporan').on('click', function(){
    $('#modal-default').modal({
      show: true
    });
    $('#id').val('');
    $('.modal-title').html('<i class="fas fa-file"></i> Tambah Laporan');
    $('#username').attr('disabled', false);
    $('#password').attr('disabled', false);
    $("#judul").val('');
    $("#deskripsi").summernote('reset');
    $("#tanggal").val('');
    $("#jenis").val('');
    $("#url").val('');

  });

  $('#save-pengaduan').on('click', function(){
    savedata(st);
  });

  loaddata();

  $('#kota').change(function (e) {
    let kotanya = this.value
    let tpstnya = '<option>Pilih Nama TPST *</option>'
    
    if(kotanya == 'Kabupaten Bandung'){
        tpstnya += `<option kota="Kabupaten Bandung " value="Cicukang Oxbow" >Cicukang Oxbow</option>
                    <option kota="Kabupaten Bandung " value="Citaliktik" >Citaliktik</option>
                    <option kota="Kabupaten Bandung " value="Ex TPA Babakan" >Ex TPA Babakan</option>
                    <option kota="Kabupaten Bandung " value="Jelekong" >Jelekong</option>
                    <option kota="Kabupaten Bandung " value="RPH Baleendah" >RPH Baleendah</option>
                    <option kota="Kabupaten Bandung " value="UPT BLK Disnaker" >UPT BLK Disnaker</option>
                    <option kota="Kabupaten Bandung " value="Wargamekar" >Wargamekar</option>`
    }
    
    if(kotanya == 'Kabupaten Bandung Barat'){
        tpstnya += `<option kota="Kabupaten Bandung Barat" value="Batujajar Timur" >Batujajar Timur</option>
                    <option kota="Kabupaten Bandung Barat" value="Cilame" >Cilame</option>
                    <option kota="Kabupaten Bandung Barat" value="Desa Suntenjaya" >Desa Suntenjaya</option>
                    <option kota="Kabupaten Bandung Barat" value="EX TPA Pasir Buluh" >EX TPA Pasir Buluh</option>
                    <option kota="Kabupaten Bandung Barat" value="Komplek Pemda" >Komplek Pemda</option>
                    <option kota="Kabupaten Bandung Barat" value="Komplek Perumahan Kota Baru Parahyangan" >Komplek Perumahan Kota Baru Parahyangan</option>`
    }

    if(kotanya == 'Kabupaten Bekasi'){
        tpstnya += `<option kota="Kabupaten Bekasi" value="Bantarjaya" >Bantarjaya</option>
                    <option kota="Kabupaten Bekasi" value="Kertamukti" >Kertamukti</option>
                    <option kota="Kabupaten Bekasi" value="Lenggahjaya" >Lenggahjaya</option>
                    <option kota="Kabupaten Bekasi" value="Sindangjaya" >Sindangjaya</option>
                    <option kota="Kabupaten Bekasi" value="Kedungwaringin" >Kedungwaringin</option>`
    }

    if(kotanya == 'Kabupaten Cianjur'){
        tpstnya += `<option kota="Kabupaten Cianjur" value="Bojong" >Bojong</option>
        <option kota="Kabupaten Cianjur" value="Mekarsari" >Mekarsari</option>
        <option kota="Kabupaten Cianjur" value="Bojongpicung" >Bojongpicung</option>
        <option kota="Kabupaten Cianjur" value="Pasirembung" >Pasirembung</option>`
    }

    if(kotanya == 'Kabupaten Karawang'){
        tpstnya += `<option kota="Kabupaten Karawang" value="Cirejag" >Cirejag</option>
        <option kota="Kabupaten Karawang" value="Jayakerta" >Jayakerta</option>
        <option kota="Kabupaten Karawang" value="Leuwisisir" >Leuwisisir</option>
        <option kota="Kabupaten Karawang" value="Mekarjati" >Mekarjati</option>`
    }

    if(kotanya == 'Kabupaten Purwakarta'){
        tpstnya += `<option kota="Kabupaten Purwakarta" value="Cikolotok" >Cikolotok</option>
        <option kota="Kabupaten Purwakarta" value="Nagrak" >Nagrak</option>
        <option kota="Kabupaten Purwakarta" value="Neglasari" >Neglasari</option>
        <option kota="Kabupaten Purwakarta" value="Sukatani" >Sukatani</option>
        <option kota="Kabupaten Purwakarta" value="Tehalsari" >Tehalsari</option>`
    }

    if(kotanya == 'Kota Bandung'){
        tpstnya += `<option kota="Kota Bandung" value="EX-TPA Cicabe" >EX-TPA Cicabe</option>
        <option kota="Kota Bandung" value="Nyengseret" >Nyengseret</option>
        <option kota="Kota Bandung" value="Tegalega" >Tegalega</option>`
    }

    if(kotanya == 'Kota Cimahi'){
        tpstnya += `<option kota="Kota Cimahi" value="Kolonel Masturi" >Kolonel Masturi</option>
        <option kota="Kota Cimahi" value="Lebak Saat Cipageran" >Lebak Saat Cipageran</option>`
    }

    if(kotanya == 'Kota Denpasar'){
        tpstnya += `<option kota="Kota Denpasar" value="Kesiman Kertalangu" >Kesiman Kertalangu</option>
        <option kota="Kota Denpasar" value="Padang Sambian" >Padang Sambian</option>
        <option kota="Kota Denpasar" value="Tahura" >Tahura</option>`
    }

    $('#tpst').html(tpstnya)
})

});

    function loaddata(){
      
      $.ajax({
          type: 'post',
          dataType: 'json',
          url: 'getglobal',
          data : {
                  param       : 'data_pengaduan',
                  type        : 'pengaduan',
           },
          success: function(result){
            
            if(result.code == 1){
                  var dt = $('#listlaporan').DataTable({
                    destroy: true,
                    paging: true,
                    lengthChange: false,
                    searching: true,
                    ordering: true,
                    info: true,
                    autoWidth: false,
                    responsive: false,
                    pageLength: 10,
                    aaData: result.data,
                      aoColumns: [
                          { 'mDataProp': 'id', 'width':'5%'},
                          { 'mDataProp': 'name'},
                          { 'mDataProp': 'email'},
                          { 'mDataProp': 'instansi'},
                          { 'mDataProp': 'kota'},
                          { 'mDataProp': 'tpst'},
                          { 'mDataProp': 'create_date'},
                          { 'mDataProp': 'pengaduan'},
                          { 'mDataProp': 'id'},
                      ],
                      order: [[0, 'ASC']],
                      aoColumnDefs:[
                          {
                              mRender: function (data, type, row){
                                  var $rowData = '';
                                  var date = new Date(data)
                                  var year = date.getFullYear()
                                  var month = ("0" + (date.getMonth() + 1)).slice(-2)
                                  var day = ("0" + date.getDate()).slice(-2)
                                  $rowData = day + '-' + month + '-' + year;
                                  return $rowData;
                              },
                              aTargets: [6]
                          },
                          {
                              mRender: function (data, type, row){
                                  var $rowData = '';
                                   
                                  $rowData += `<button class="btn btn-success btn-xs" onClick="editdong('${row.id}', '${row.name}', '${row.email}', '${row.instansi}', '${row.kota}', '${row.tpst}', '${row.create_date}', '${row.pengaduan}')"><i class="fa fa-edit"></i> Edit</button>`;
                                  // $rowData += `<button class="btn btn-default btn-xs" onClick="lihat('${row.pengaduan}')"><i class="fa fa-eye"></i> Lihat</button>`;
                                  $rowData += `<button class="btn btn-danger btn-xs" onClick="deleteData(${row.id})"><i class="fa fa-trash"></i> Hapus</button>`;
                                  
                                  return $rowData;
                              },
                              aTargets: [8]
                          },
                      ],
    
                      fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                          var index = iDisplayIndexFull + 1;
                          $('td:eq(0)', nRow).html(' '+index);
                          return  ;
                      },
    
                      fnInitComplete: function () {
                          var that = this;
                          var td ;
                          var tr ;
    
                          this.$('td').click( function () {
                              td = this;
                          });
                          this.$('tr').click( function () {
                              tr = this;
                          });
    
    
                          $('#listproj_filter input').bind('keyup', function (e) {
                              return this.value;
                          });
    
                      }
                  });
              }else{
                $('#listlaporan').DataTable()
              }
    
          }
      });
    }

    function savedata(st){

      var formData = new FormData();
      formData.append('id', $('#id').val())
      formData.append('name', $('#name').val())
      formData.append('email', $('#email').val())
      formData.append('instansi', $('#instansi').val())
      formData.append('kota', $('#kota').val())
      formData.append('tpst', $('#tpst').val())
      formData.append('pengaduan', $('#pengaduan_nya').val())
      formData.append('create_date', $('#tanggal_dong').val())

      var stat;
        switch (st) {
          case false:
              stat = '0';
            break;
          default:
              stat = '1'
        }

        if($('#id').val()){
          var baseurl = 'updatepengaduan';
          var msg = 'Update Pengaduan';

        }else{
          var baseurl = 'saveLaporan';
          var msg = 'Tambah Laporan';
        }

        $.ajax({
          type: 'post',
          url: baseurl,
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          data: formData,
          async:false,
            success: function(result){
              Swal.fire({
                title: 'Sukses!',
                text: msg,
                icon: 'success',
                showConfirmButton: false,
                timer: 1500
              });

              $('#modal-default').modal('hide');
              loaddata();
            }
          });
        };

function editdong( id, name, email, instansi, kota, tpst, create_date, pengaduan){
  $('[name="pengaduan-input"]').val('')
  $('#kota').val('').trigger("change")
  $('#tpst').val('').trigger("change")
  var date = new Date(create_date);

  var day = date.getDate();
  var month = date.getMonth() + 1;
  var year = date.getFullYear();
  if (month < 10) month = "0" + month;
  if (day < 10) day = "0" + day;
  var today = year + "-" + month + "-" + day; 
  
  $('#modal-default').modal('show');
  $('.modal-title').html('Edit Lapaoran');
  $('#id').val(id);
  $('#name').val(name)
  $('#email').val(email)
  $('#instansi').val(instansi)
  $('#kota').val(kota).trigger("change")
  $('#tpst').val(tpst).trigger("change")
  $('#pengaduan_nya').val(pengaduan)
  $('#tanggal_dong').val(today)

  // document.getElementById("tanggal_dong").value = "2022-05-31";
  // $('#deskripsi').summernote('code', deskripsi);
  // $('#tanggal').val(new Date(tanggal));
  // $('#jenis').val(jenis);
  // $('#url').val(url);

}

function deleteData(id)
{
  
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success btn-sm swal2-styled-custom',
      cancelButton: 'btn btn-danger btn-sm swal2-styled-custom'
    },
    buttonsStyling: false
  });

  swalWithBootstrapButtons.fire({
    title: 'Anda Yakin, hapus Pengaduan ini?',
    text: "",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: '<i class="fas fa-check"></i> Ya',
    cancelButtonText: '<i class="fas fa-times"></i> Tidak',
    reverseButtons: true
  }).then((result) => {
  if (result.isConfirmed) {
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: 'deletepengaduan',
      data : {
              id    : id,
            },
      success: function(data)
      {
        Swal.fire({
          title: 'Sukses!',
          text: 'Hapus Pengaduan',
          icon: 'success',
          showConfirmButton: false,
          timer: 1500
        });
        loaddata();
      }
    });
  }
})

}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var blah = input.id.replace("image", "blah");
    reader.onload = function(e) {
      $('#'+blah).attr('src', e.target.result);
      window.img = e.target.result;
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function modaldetail(id,username,role,status,name,foto){
    $('#modal-detail').modal({
      show: true
    });

    $('.modal-title').html('Detail');

    var stt = '';
    if(status == 1){
      stt +=`<span class="badge badge-primary right">Aktif</span>`;
    }else{
      stt +=`<span class="badge badge-warning right">Non Aktif</span>`;
    }

    $('#detail-foto').attr('src', foto);
    $('#detail-name').text(name);
    $('#detail-username').html('username: <i>'+username+'</i>');
    $('#detail-status').html(stt);
    $('#detail-role').text(role);
}


function lihat(data){
  $('#lihat-pengaduan').modal('show');
  $('#ini-pengaduan').html(data);
}