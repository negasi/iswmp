$(function () {

  console.log('You are running jQuery version: ' + $.fn.jquery);
  $('[name="summernote"]').summernote({
    height: 200,   //set editable area's height
    toolbar: [],
    codemirror: { // codemirror options
      theme: 'monokai'
    }
  });

  $('.select2').select2();
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })

  //Date picker
  $('#reservationdate1').datetimepicker({
    format: 'L'
  });
  $('#reservationdate2').datetimepicker({
    format: 'L'
  });
  $('#reservationdate3').datetimepicker({
    format: 'L'
  });

  var st = true;
  window.img = '';
  $("#stat").each(function(){
    // $(this).bootstrapSwitch('state', $(this).prop('checked'));
    $(this).bootstrapSwitch({
      onSwitchChange: function(e, state) {
        st = state;
      }
    });
  });

  $('#listberita').DataTable();
  
  $('#modal-default').on('show.bs.modal', function(){
  })

  $('.bootstrap-switch-handle-on').html('Ya');
  $('.bootstrap-switch-handle-off').html('Tidak');

  $('#lelang > a').attr('class','nav-link active');

  $('#add-lelang').on('click', function(){
    $('.modal-title').html('<i class="fas fa-file-signature"></i> Tambah Lelang');
    $('#judul_paket').val('')
    $('#pagu_hps').val('')
    $('#hps').val('')
    $('#pel_paket').val('')
    $('#target_lelang').select2("val", "0")
    $('#tanggal_mulai').val('')
    $('#tanggal_akhir').val('')
    $('#tanggal_akhir_pekerjaan').val('')
    $('#tanggal_kontrak').val('')
    $('#status').select2("val", "0")
    $('#link').val('')
    $('#deskripsi').summernote('reset');
    $('#rekomendasi').val('')
    $('#tahun_lelang').select2("val", "0")
    $('#metode_lelang').select2("val", "-")
    $('#jenis_lelang').select2("val", "-")
    $('#keterangan').val('')
    $('#nilai_tayang').val('')
    $('#nama_pelaksana').val('')

    $('#blah_1').attr('src', 'assets/img/no-image.png');

    $('#modal-default').modal({
      show: true
    });

    $('#id').val('');
    $('#idfile').val('');
  });

  $('#save-berita').on('click', function(){
    
    savedata(st);
  });

  loaddata();

  $("[name='image_input']").on('change',function() {
    readURL(this);
  });


  $("#stat").bootstrapSwitch();

  $("#stat").on('switchChange.bootstrapSwitch', function (event, state) {
    
  });

  
$(".select2bs4").change(function () {
    if($(".select2bs4 option:selected").length > 1) {

    }
});

$("#tag").on('keyup',function () {
alert();
});



});

function loaddata(){
  
  $.ajax({
      type: 'post',
      dataType: 'json',
      url: 'getglobalwhereby',
      data : {
              param       : 'data_lelang',
              type        : 'lelang',
       },
      success: function(result){
        
        if(result.code == 1){
              var dt = $('#listlelang').DataTable({
                destroy: true,
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: false,
                responsive: false,
                pageLength: 10,
                aaData: result.data,
                  aoColumns: [
                      { 'mDataProp': 'id', 'width':'5%'},
                      { 'mDataProp': 'judul_paket'},
                      { 'mDataProp': 'pagu_hps'},
                      { 'mDataProp': 'hps'},
                      { 'mDataProp': 'pelaksana'},
                      { 'mDataProp': 'nilai_tayang'},
                      { 'mDataProp': 'nama_pelaksana'},
                      { 'mDataProp': 'target_lelang'},
                      { 'mDataProp': 'metode_lelang'},
                      { 'mDataProp': 'jenis_lelang'},
                      { 'mDataProp': 'keterangan'},
                      { 'mDataProp': 'tanggal_mulai'},
                      { 'mDataProp': 'tanggal_akhir'},
                      { 'mDataProp': 'tanggal_akhir_pekerjaan'},
                      { 'mDataProp': 'status'},
                      { 'mDataProp': 'tahun'},
                      { 'mDataProp': 'id'},
                  ],
                  order: [[0, 'ASC']],
                  aoColumnDefs:[
                    {
                      mRender: function (data, type, row){
                        if(type == 'display'){
                          var $rowData = '';
                          if(data){
                            var bilangan = data;
                            var	number_string = bilangan.toString(),
                                sisa 	= number_string.length % 3,
                                rupiah 	= number_string.substr(0, sisa),
                                ribuan 	= number_string.substr(sisa).match(/\d{3}/g);
                              
                            if (ribuan) {
                              separator = sisa ? '.' : '';
                              rupiah += separator + ribuan.join('.');
                              $rowData = 'Rp.'+rupiah;
                            }
                          }
                          return $rowData;
                        }
                        return data;
                      },
                      aTargets: [2,3,5]
                    },
                    {
                      mRender: function (data, type, row){
                        if(type == 'display'){
                          var $rowData = '';
                          var bul = ['-', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
                          
                          if(data !== null){
                            $rowData =  bul[data] + ' ' + row.tahun
                          }
                          return $rowData;
                        }
                        return data;
                      },
                      aTargets: [7]
                    },
                    {
                      mRender: function (data, type, row){
                        if(type == 'display'){
                          var $rowData = '';
                          var bul = ['-', 'Belum Tayang','Sudah Tayang','Terkontrak (pelaksanaan)']
                          
                          if(data !== null){
                            $rowData =  typeof bul[data] != 'undefined' || bul[data] != null ? bul[data] : '-'
                          }
                          return `<span class="right badge badge-${data == 1 ? 'danger' : data == 2 ? 'warning' : data == 3 ? 'success' : 'default' }">${$rowData}</span>`;
                        }
                        return data;
                      },
                      aTargets: [14]
                    },
                    {
                      mRender: function (data, type, row){

                          var file = ''
                          var idfile = ''
                          for( var key in row.files ) {
                            file = row.files[key].path+'/'+row.files[key].filename;
                            idfile = row.files[key].id;
                          }
                         var $rowData = '';
                              $rowData += `
                              <div class="btn-group btn-group-sm">
                              <button type="button" class="btn btn-info">Action</button>
                              <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown">
                                <span class="sr-only">Toggle Dropdown</span>
                              </button>
                              <div class="dropdown-menu" role="menu">
                                <a class="dropdown-item" href="#" onclick="editdong(${row.id}, '${row.judul_paket}', '${row.pelaksana}', '${row.tanggal_mulai}', '${row.tanggal_akhir}', '${row.status}', '${row.tanggal_kontrak}', '${row.deskripsi}', '${row.rekomendasi}', '${row.tahun}', '${row.link}', '${row.pagu_hps}', '${idfile}', '${file}', '${row.hps}', '${row.target_lelang}', '${row.metode_lelang}', '${row.keterangan}', '${row.nilai_tayang}', '${row.nama_pelaksana}', '${row.jenis_lelang}', '${row.tanggal_akhir_pekerjaan}')"><i class="far fa-edit"></i> Edit</a>
                                <a class="dropdown-item" href="#" onclick="deleteData(${row.id}, '${idfile}', '${file}')"><i class="far fa-trash-alt"></i> Hapus</a>
                              </div>
                            </div>`;

                          return $rowData;
                      },
                      aTargets: [16]
                    }
                  ],

                  fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                      var index = iDisplayIndexFull + 1;
                      $('td:eq(0)', nRow).html(' '+index);
                      return  ;
                  },

                  fnInitComplete: function () {
                      var that = this;
                      var td ;
                      var tr ;

                      this.$('td').click( function () {
                          td = this;
                      });
                      this.$('tr').click( function () {
                          tr = this;
                      });

                  }
              });
          }

      }
  });
}

    function savedata(st){
      var img = window.img;
      var id = $('#id').val();
      var idfile = $('#idfile').val();

      var judul_paket     = $('#judul_paket').val()
      var pelaksana       = $('#pel_paket').val()
      var target_lelang   = $('#target_lelang').val()
      var tanggal_mulai   = $('#tanggal_mulai').val()
      var tanggal_akhir   = $('#tanggal_akhir').val()
      var tanggal_akhir_pekerjaan   = $('#tanggal_akhir_pekerjaan').val()
      // var tanggal_kontrak = $('#tanggal_kontrak').val()
      var status          = $('#status').val()
      var link            = $('#link').val()
      // var deskripsi       = $('#deskripsi').val();
      // var rekomendasi     = $('#rekomendasi').val()
      var tahun           = $('#tahun_lelang').val()
      var pagu_hps           = $('#pagu_hps').val()
      var hps               = $('#hps').val()
      var metode_lelang               = $('#metode_lelang').val()
      var jenis_lelang               = $('#jenis_lelang').val()
      var keterangan               = $('#keterangan').val()
      var nilai_tayang               = $('#nilai_tayang').val()
      var nama_pelaksana               = $('#nama_pelaksana').val()
      
      var formData = new FormData();
      formData.append('judul_paket', judul_paket)
      formData.append('pelaksana', pelaksana)
      formData.append('target_lelang', target_lelang)
      formData.append('tanggal_mulai', tanggal_mulai)
      formData.append('tanggal_akhir', tanggal_akhir)
      formData.append('tanggal_akhir_pekerjaan', tanggal_akhir_pekerjaan)
      // formData.append('tanggal_kontrak', tanggal_kontrak)
      formData.append('status', status)
      formData.append('link', link)
      // formData.append('deskripsi', deskripsi)
      // formData.append('rekomendasi', rekomendasi)
      formData.append('tahun', tahun)      
      formData.append('pagu_hps', pagu_hps)    
      formData.append('hps', hps)    
      formData.append('metode_lelang', metode_lelang)    
      formData.append('jenis_lelang', jenis_lelang)    
      formData.append('keterangan', keterangan)    
      formData.append('nilai_tayang', nilai_tayang)    
      formData.append('nama_pelaksana', nama_pelaksana)    

      for (let index = 0; index < $("[name='image_input']").length; index++) {
        var src = $("[name='image_input']")[index].files[0];
        
        formData.append('files[]', src);
      }
      

        if(id){
          formData.append('id', id) 
          formData.append('idfile', idfile);
          var baseurl = 'updatedatalelang';
          var msg = 'Update Lelang';

        }else{
          var baseurl = 'savedatalelang';
          var msg = 'Tambah Lelang';
        }

        $.ajax({
          type: 'post',
          url: baseurl,
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          data: formData,
          async:false,
            success: function(result){
              Swal.fire({
                title: 'Sukses!',
                text: msg,
                icon: 'success',
                showConfirmButton: false,
                timer: 1500
              });

              $('#modal-default').modal('hide');
              loaddata();
            }
          });
    };

function editdong(id, judul_paket, pelaksana, tanggal_mulai, tanggal_akhir, status, tanggal_kontrak, deskripsi, rekomendasi, tahun, link, pagu, idfile, file, hps, target_lelang, metode_lelang, keterangan, nilai_tayang, nama_pelaksana, jenis_lelang, tanggal_akhir_pekerjaan){
  $('#add-lelang').trigger('click');
  $('.modal-title').html('<i class="fas fa-file-signature"></i> Edit Lelang');
  $('#id').val(id);
  
  $('#judul_paket').val(judul_paket)
  $('#pel_paket').val(pelaksana).trigger("change")
  $('#target_lelang').val(target_lelang).trigger("change")
  $('#tanggal_mulai').val(tanggal_mulai)
  $('#tanggal_akhir').val(tanggal_akhir)
  $('#tanggal_akhir_pekerjaan').val(tanggal_akhir_pekerjaan)
  $('#tanggal_kontrak').val(tanggal_kontrak)
  $('#status').val(status).trigger("change")
  $('#link').val(link)
  $('#deskripsi').summernote('code', deskripsi);
  $('#rekomendasi').val(rekomendasi)
  $('#pagu_hps').val(pagu)
  $('#hps').val(hps)
  $('#tahun_lelang').val(tahun).trigger("change")
  $('#metode_lelang').val(metode_lelang).trigger("change")
  $('#jenis_lelang').val(jenis_lelang).trigger("change")
  $('#keterangan').val(keterangan)
  $('#nilai_tayang').val(nilai_tayang)
  $('#nama_pelaksana').val(nama_pelaksana)

  $('#idfile').val(idfile);
  $('#blah_1').attr('src', file);

}

function deleteData(id, idfile, file)
{
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: 'btn btn-success btn-sm swal2-styled-custom',
      cancelButton: 'btn btn-danger btn-sm swal2-styled-custom'
    },
    buttonsStyling: false
  });

  swalWithBootstrapButtons.fire({
    title: 'Anda yakin, hapus data ini?',
    text: "",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: '<i class="fas fa-check"></i> Ya',
    cancelButtonText: '<i class="fas fa-times"></i> Tidak',
    reverseButtons: true
  }).then((result) => {
  if (result.isConfirmed) {
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: 'deletelelang',
      data : {
              id    : id,
              id_file    : idfile,
              path    : file,
            },
      success: function(data)
      {
        Swal.fire({
          title: 'Sukses!',
          text: 'Hapus Lelang',
          icon: 'success',
          showConfirmButton: false,
          timer: 1500
        });
        loaddata();
      }
    });
  }
})

}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var blah = input.id.replace("image", "blah");
    reader.onload = function(e) {
      $('#'+blah).attr('src', e.target.result);
      window.img = e.target.result;
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function modaldetail(id,username,role,status,name,foto){
    $('#modal-detail').modal({
      show: true
    });

    $('.modal-title').html('Detail');

    var stt = '';
    if(status == 1){
      stt +=`<span class="badge badge-primary right">Aktif</span>`;
    }else{
      stt +=`<span class="badge badge-warning right">Non Aktif</span>`;
    }

    $('#detail-foto').attr('src', foto);
    $('#detail-name').text(name);
    $('#detail-username').html('username: <i>'+username+'</i>');
    $('#detail-status').html(stt);
    $('#detail-role').text(role);
}


    function addgambar(){
      var count = $("#gambar-container").children().length + 1;

      var elemen = `<div class="col-md-4">
      <div class="card card-light card-outline">
        <div class="card-tools">
          <a onclick="$(this).parent().parent().parent().remove()" class="btn btn-tool">
            <i class="fas fa-times"></i>
          </a>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label></label>
            <div class="text-center">
                <img id="blah_`+count+`" name="images" class="img-fluid" src="assets/img/no-image.png" alt="picture">
                <canvas hidden id="myCanvas_`+count+`"/>
            </div>
            <div class="custom-file" style="margin-bottom: 10px;margin-top: 10px;">
              <input type="file" class="custom-file-input" id="image_`+count+`" name="image_input" onChange="pilihgambar(this)">
              <label class="custom-file-label" for="image_`+count+`">Pilih foto</label>
            </div>
          <input id="caption_`+count+`" name="caption" type="text" class="form-control" placeholder="Caption">
          </div>
        </div>
      </div>
    </div>`;
    $("#gambar-container").append(elemen);
      
    }

    function pilihgambar(ini){
      readURL(ini);
    }

    function updatepublish(id,stat){
      var formData = new FormData();
      formData.append('id', id);
      formData.append('stat', stat);
      
      $.ajax({
        type: 'post',
        url: 'updateberita',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: formData,
        async:false,
          success: function(result){
            Swal.fire({
              title: 'Sukses!',
              text: 'Berita telah di publish',
              icon: 'success',
              showConfirmButton: false,
              timer: 1500
            });

            $('#modal-default').modal('hide');
            loaddata();
          }
        });
    }