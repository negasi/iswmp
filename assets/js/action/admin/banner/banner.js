$(function () {

  console.log('You are running jQuery version: ' + $.fn.jquery);
  $('#summernote').summernote({
    height: 200,   //set editable area's height
    codemirror: { // codemirror options
      theme: 'monokai'
    }
  });
  $('.select2').select2();
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })

  //Date picker
  $('#reservationdate').datetimepicker({
    format: 'L'
  });

  var st = true;
  window.img = '';
  $("input[data-bootstrap-switch]").each(function(){
    // $(this).bootstrapSwitch('state', $(this).prop('checked'));
    $(this).bootstrapSwitch({
      onSwitchChange: function(e, state) {
        st = state;
      }
    });
  });

  $('#listbanner').DataTable();
  
  $('#modal-default').on('hidden.bs.modal', function(){
    $('#gambar-container').html('')
    $('#blah_1').attr('src', 'assets/img/no-image.png')
  })

  $("[name='image_input']").on('change',function() {
    readURL(this);
  });

  $('.bootstrap-switch-handle-on').html('Ya');
  $('.bootstrap-switch-handle-off').html('Tidak');

  $('#banner > a').attr('class','nav-link active');
  $('#banner').attr('class','nav-item menu-is-opening menu-open');
  // $('#foto > a').attr('class','nav-link active');
  // $('#foto > a > i').addClass('text-info');

  loaddata();

  $('#add-banner').on('click', function(){
    $('.add-gambarnya').show()
    $('#modal-default').modal({
      show: true
    });
    $('#id').val('');
    $('#id_file').val('');
    $('#keterangan').val('');
    $('#judul').val('');
    $('#link').val('');
    $('.custom-file-label').html('');
    $('.modal-title').html('<i class="fas fa-file"></i> Tambah Banner');
    $('#blah_1').attr('src', 'assets/img/no-image.png');
    $('label[for="foto-user"]').text('Pilih Foto');
  });

  $('#save-banner').on('click', function(){
    
    savedata(st);
  });

});


function loaddata(){

  $.ajax({
      type: 'post',
      dataType: 'json',
      url: 'getglobal',
      data : {
              param       : 'data_banner',
              type        : 'banner',
       },
      success: function(result){
        
        if(result.code == 1){
              var dt = $('#listbanner').DataTable({
                destroy: true,
                paging: true,
                lengthChange: false,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: false,
                responsive: false,
                pageLength: 10,
                aaData: result.data,
                  aoColumns: [
                      { 'mDataProp': 'id', 'width':'5%'},
                      { 'mDataProp': 'id'},
                      { 'mDataProp': 'judul'},
                      { 'mDataProp': 'link'},
                      { 'mDataProp': 'bulan'},
                      { 'mDataProp': 'id'},
                  ],
                  order: [[0, 'ASC']],
                  aoColumnDefs:[
                      {
                          mRender: function (data, type, row){
                              var $rowData = '';
                              for( var key in row.files ) {
                                $rowData += `
                                  <div class="card">
                                    <img id="" name="" class="img-fluid" src="`+row.files[key].path+'/'+row.files[key].filename+`" alt="">
                                  </div>
                                  `;
                              }
                              
                              return $rowData;
                          },
                          aTargets: [1]
                      },
                      {
                        mRender: function (data, type, row){
                          var mydate = new Date(row.create_date);
                          var date = ("0" + mydate.getDate()).slice(-2);
                          var month = ("0" + (mydate.getMonth() + 1)).slice(-2);
                          var year = mydate.getFullYear();
                          var str = date+'/'+month+'/'+year;

                          var stat = row.stat;
                          if(stat == 1){
                            var st = 'Publish'
                            var tex = 'text-success';
                          }else{
                            var st = 'No Publish'
                            var tex = 'text-danger';
                          }
                          var $rowData = '';
                                $rowData += `<div class="card">
                                <div class="card-body">
                                    <div class="d-flex justify-content-between">
                                    <p class="text-success text-sm">
                                      <i class="far fa-user"></i>
                                    </p>
                                    <p class="d-flex flex-column">
                                      <span class="text-muted"> `+row.username+`</span>
                                    </p>
                                  </div>
                                  <div class="d-flex justify-content-between">
                                    <p class="text-primary text-sm">
                                      <i class="far fa-calendar-alt"></i>
                                    </p>
                                    <p class="d-flex flex-column">
                                      <span class="text-muted"> `+str+`</span>
                                    </p>
                                  </div>
                                  <div class="d-flex justify-content-between">
                                    <p class="`+tex+` text-sm">
                                      <i class="fas fa-sign-in-alt"></i>
                                    </p>
                                    <p class="d-flex flex-column ">
                                      <span class="text-muted">`+st+`</span>
                                    </p>
                                  </div>
                                </div>
                              </div>`;

                            return $rowData;
                        },
                        aTargets: [4]
                    },
                      {
                          mRender: function (data, type, row){
                            console.log();
                            
                              var $rowData = '';
                                  $rowData += `
                                  <div class="btn-group">
                                  <button type="button" class="btn btn-info">Action</button>
                                  <button type="button" class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="true">
                                    <span class="sr-only">Toggle Dropdown</span>
                                  </button>
                                  <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(68px, -165px, 0px);">
                                    <a class="dropdown-item" href="#" onclick="editdata(`+row.id+`, '${row.judul}', '${row.link}', '${row.keterangan}', '${'files' in row ? JSON.stringify(row.files).replace(/\"/g, '|') : JSON.stringify([]) }')"><i class="far fa-edit"></i> Edit</a>
                                    <a class="dropdown-item" href="#" onclick="deleteData(`+row.id+`, '${'files' in row ? JSON.stringify(row.files).replace(/\"/g, '|') : JSON.stringify([]) }')"><i class="far fa-trash-alt"></i> Hapus</a>
                                    <div class="dropdown-divider"></div>
                                    <a hidden class="dropdown-item" href="#"><i class="fas fa-sign-out-alt"></i> Tidak Tayang</a>
                                  </div>
                                </div>
                                              `;

                              return $rowData;
                          },
                          aTargets: [5]
                      }
                  ],

                  fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull){
                      var index = iDisplayIndexFull + 1;
                      $('td:eq(0)', nRow).html(' '+index);
                      return  ;
                  },

                  fnInitComplete: function () {
                      var that = this;
                      var td ;
                      var tr ;

                      this.$('td').click( function () {
                          td = this;
                      });
                      this.$('tr').click( function () {
                          tr = this;
                      });


                      $('#listproj_filter input').bind('keyup', function (e) {
                          return this.value;
                      });

                  }
              });
          }else{
            $('#listbanner').DataTable().clear().draw();
          }

      }
  });
}

function savedata(st){
  var img = window.img;
  var id = $('#id').val();
  var id_file = $('#id_file_1').val();
  var judul = $('#judul').val();
  var link = $('#link').val();
  var stat = $('#stat').val();
  var keterangan = $('#keterangan').val();

  var formData = new FormData();
  formData.append('id', id);
  formData.append('judul', judul);
  formData.append('link', link);
  formData.append('stat', stat);
  formData.append('keterangan', keterangan);

  var iscapt = [];
  for (let index = 0; index < $("[name='image_input']").length; index++) {
    var src = $("[name='image_input']")[index].files[0];
    
    formData.append('files[]', src);
    if(id){
      // var idfile = $($("[name='image_input']")[index]).attr('id-file-untuk-upload');
      // formData.append('id_file[]', idfile);
    }
  }
  
  var stat;
    switch (st) {
      case false:
          stat = '0';
        break;
      default:
          stat = '1'
    }

    if(id){
      // formData.append('id_file', id_file);
      var baseurl = 'updateBanner';
      var msg = 'Update Banner';

    }else{
      var baseurl = 'savedatabanner';
      var msg = 'Tambah Banner';
    }

    $.ajax({
      type: 'post',
      url: baseurl,
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: formData,
      async:false,
        success: function(result){
          Swal.fire({
            title: 'Sukses!',
            text: msg,
            icon: 'success',
            showConfirmButton: false,
            timer: 1500
          });

          $('#modal-default').modal('hide');
          loaddata();
        }
      });
    };

function edituser(id, username, password, status, role, name, foto){
  $('#add-users').trigger('click');
  $('.modal-title').html('Edit User');
  $('#id').val(id);
  $('#name').val(name);
  $('#username').val(username);
  $('#username').attr('disabled', true);
  $('#password').val(password);
  $('#password').attr('disabled', true);
  let fot = foto.split("/");
  $('label[for="foto-user"]').text(fot[fot.length - 1]);
  $('#blah').attr('src', foto);
  $("#stat").bootstrapSwitch('state', status == '1' ? true : false);

  if(role == '10'){
    $("#super-admin").attr('checked', true).trigger('click');
  }else{
    $("#admin").attr('checked', true).trigger('click');
  }
}

function deleteData(id, path)
{

    let file = JSON.parse(path.replace(/\|/g, '"'))
    var id_file = ''
    var pathh = ''
    for (let i = 0; i < file.length; i++) {
      const fl = file[i];
      id_file    =  fl['id']
      pathh      =  fl['path']+'/'+fl['filename']
    }
    
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success btn-sm swal2-styled-custom',
        cancelButton: 'btn btn-danger btn-sm swal2-styled-custom'
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons.fire({
      title: 'Anda yakin, hapus Banner ini?',
      text: "",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: '<i class="fas fa-check"></i> Ya',
      cancelButtonText: '<i class="fas fa-times"></i> Tidak',
      reverseButtons: true
    }).then((result) => {
    if (result.isConfirmed) {
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: 'deletebanner',
        data : {
                id    : id,
                path : pathh,
                id_file : id_file
              },
        success: function(data)
        {
          Swal.fire({
            title: 'Sukses!',
            text: 'Hapus Banner',
            icon: 'success',
            showConfirmButton: false,
            timer: 1500
          });
          loaddata();
        }
      });
    }
  })

}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    var blah = input.id.replace("image", "blah");
    reader.onload = function(e) {
      $('#'+blah).attr('src', e.target.result);
      window.img = e.target.result;
    }
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function modaldetail(id,username,role,status,name,foto){
    $('#modal-detail').modal({
      show: true
    });

    $('.modal-title').html('Detail');

    var stt = '';
    if(status == 1){
      stt +=`<span class="badge badge-primary right">Aktif</span>`;
    }else{
      stt +=`<span class="badge badge-warning right">Non Aktif</span>`;
    }

    $('#detail-foto').attr('src', foto);
    $('#detail-name').text(name);
    $('#detail-username').html('username: <i>'+username+'</i>');
    $('#detail-status').html(stt);
    $('#detail-role').text(role);
}

function cekusername(uname){
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: 'cekusername',
        data : {
                username      : uname,
         },
        success: function(result){
            console.log(result);
            if(result){
              $('#username').attr('class', 'form-control is-invalid');
              $('#warning').attr('style', 'color: #f9b2b2;display:block;');
              $('#lbl-unm').attr('style', 'display:none;');
              $('#save-user').attr('disabled', true);

            }
        }
      });
    };


    function addgambar(mode){
      $('.add-gambarnya').hide()
      var count = $("#gambar-container").children().length + 1;
      if(count <= 5 ){
        var elemen = `<div class="col-md-4">
                        <div class="card card-light card-outline">
                          <div class="card-tools">
                            <a onclick="removefile(this, ${mode})" id="btn_remove_${count}" class="btn btn-tool remove-file" hidden>
                              <i class="fas fa-times"></i>
                            </a>
                          </div>
                          <div class="card-body">
                            <div class="form-group">
                              <label></label>
                              <div class="text-center">
                                  <img id="blah_`+count+`" name="images" class="img-fluid" src="assets/img/no-image.png" alt="picture">
                                  <canvas hidden id="myCanvas_`+count+`"/>
                              </div>
                              <div class="custom-file" style="margin-bottom: 10px;margin-top: 10px;">
                                <input accept=".png" type="file" class="custom-file-input" id="image_`+count+`" name="image_input" onChange="pilihgambar(this)">
                                
                                <label ${mode ? ' hidden': ''} class="custom-file-label" for="image_`+count+`">Pilih Banner</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>`;

        $("#gambar-container").append(elemen);
      }
      
    }

    function pilihgambar(ini){
      readURL(ini);
      $(ini).attr( 'id-file-untuk-upload', $(ini).attr('data-file-id'))

    }

    function editdata(id, judul, link, keterangan, files){
      let file = JSON.parse(files.replace(/\|/g, '"'))
      $('#modal-default').modal('show')
      $('#id').val(id)
      $('#judul').val(judul)
      $('#link').val(link)
      $('#keterangan').val(keterangan)

      $('.btn-tool').click()
      let ke = 1;
      for (let i = 0; i < file.length; i++) {
          addgambar(1)
          
          const fl = file[i];
          let id        =  fl['id']
          let path      =  fl['path']+'/'+fl['filename']
          $('#blah_'+ke).attr('src', path)
          // $('#image_'+ke).attr('data-file-id', `'${id}'`)
          // if(ke == 1){
          //   $('#image_'+ke).attr('onchange', `semat('${id}')`)
          // }
          
          $('#id_file_'+ke).val(id)
          $('#btn_remove_'+ke).attr('data-path', fl['path'])
          $('#btn_remove_'+ke).attr('data-filename', fl['filename'])
          $('#btn_remove_'+ke).attr('data-id', fl['id'])
          if(ke == 1){
            $('#btn_remove_1').attr('onclick', `removefile(this, 1)`)
            $('#btn_remove_1').removeAttr('hidden')
          }
          
        ke++
      }

      // $('#id_file').val(id_file)
      // $('#blah_1').attr('src', path+'/'+filename)
    } 

    function removefile(isthis, mode) {
        let name_file = $(isthis).attr('data-filename');
        let path_file = $(isthis).attr('data-path');
        let id_file   = $(isthis).attr('data-id');
        $('.add-gambarnya').show()
        if(mode){
          $.ajax({
            type: 'post',
            dataType: 'json',
            url: 'deletefilefoto',
            data : {
                    id          : id_file,
                    filename    : name_file,
                    path        : path_file,
                  },
            success: function(data)
            {
            }
          })
        }
        
        $(isthis).parent().parent().parent().remove()
    }

    function semat(id) {
      $('#image_1').attr('id-file-untuk-upload', id)
    }
