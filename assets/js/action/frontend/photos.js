console.log('You are running jQuery version: ' + $.fn.jquery);
window.slideIndex = 1;
$(function () {    
    loaddata();
})

function loaddata(){

    $.ajax({
        type: 'post',
        dataType: 'json',
        url: 'getglobal',
        data : {
                param       : 'data_foto',
                type        : 'foto',
        },
        success: function(result){
            let code = result.code;
            let data = result.data;
            let elem = '';
            for (let index = 0; index < data.length; index++) {
                elem += `<div class="tab-1 col-sm-4">
                            <div class="gallery-avatar">
                                <a href="#" onclick="showimg('${JSON.stringify(data[index]['files']).replace(/\"/g, '|')}')" data-showsocial="false"  class="html5lightbox crop" data-group="set2" title="">
                                    <img src="`+data[index]['files'][0]['path']+'/'+data[index]['files'][0]['filename']+`" alt="">
                                </a>
                            </div>
                            <h4>`+data[index]['judul']+`</h4>
                        </div>`;

            }
            $('.masonry').append(elem);


        }
    })
}

function showimg(src) {
    $('#modal-show').modal('show')
    // $('#fotonya').attr('src', src)
    // $('#judul-foto').html(judul)
    // $('#keterangan-foto').html(keterangan)

    let file = JSON.parse(src.replace(/\|/g, '"'))
    console.log(file);
    let el = ''
    file.forEach(element => {
      el +=  `<img class="mySlides" src="${element.path+'/'+element.filename}" style="width:100%">`
    });

    el += `<button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
    <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>`

    $('#to-append').html(el)
    showDivs(window.slideIndex);
}

function plusDivs(n) {
    showDivs(window.slideIndex += n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    if (n > x.length) {window.slideIndex = 1}
    if (n < 1) {window.slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
    }
    x[window.slideIndex-1].style.display = "block";  
}
